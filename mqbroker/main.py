# -*- coding: utf-8 -*-
# Copyright 2018-2019 GO FLEET SOLUTIONS S.R.L. See LICENSE for details.
"""
mqbroker entry point
"""

import argparse
import os
import sys

import zmq

from . import __version__

VERSION_INTRO = r'scanner v{}'.format(__version__)


def determine_path():
    try:
        root = __file__
        if os.path.islink(root):
            root = os.path.realpath(root)
        return os.path.dirname(os.path.abspath(root))

    except:
        print("Invalid __file__ variable.")
        sys.exit()


def parse_args():
    parser = argparse.ArgumentParser(description='Collect NMEA MQ data and publish to 0MQ sockets.')
    parser.add_argument('-s', '--src', dest='src_addrs', action='append', type=str,
                        help='0MQ socket address to listen, in the form ‘protocol://interface:port’. '
                        'You may set this option multiple times.')
    parser.add_argument('-d', '--dst', dest='dst_addrs', action='append', type=str,
                        help='0MQ socket address to publish data, in the form ‘protocol://interface:port’. '
                        'You may set this option multiple times.')
    parser.add_argument('-t', '--topic-filter', dest='topic_filter', action='append', type=str,
                        help='OMQ data topic to filter (default=\'\').')
    parser.add_argument('-v', '--verbose', dest='verbose', action='store_true',
                        help='Enable extra logging information.')
    parser.add_argument('-V', '--version', dest='show_version', action='store_true')

    return vars(parser.parse_args())


def main():
    arguments = parse_args()
    if arguments.get('show_version'):
        print(VERSION_INTRO)
        return

    verbose = arguments.get('verbose')

    topic_filter = arguments.get('topic_filter')
    src_addrs = arguments.get('src_addrs')
    dst_addrs = arguments.get('dst_addrs')
    if not src_addrs or not dst_addrs:
        return

    context = zmq.Context()

    subscriber = context.socket(zmq.SUB)
    for addr in src_addrs:
        subscriber.connect(addr)

    if isinstance(topic_filter, str):
        topic_filter = (topic_filter,)

    for topic in topic_filter:
        subscriber.setsockopt_string(zmq.SUBSCRIBE, topic)

    publisher = context.socket(zmq.PUB)
    for addr in dst_addrs:
        publisher.bind(addr)

    while True:
        try:
            message = subscriber.recv_multipart()
            publisher.send_multipart(message)

            if verbose:
                print(message)

        except KeyboardInterrupt:
            break


if __name__ == '__main__':
    main()
