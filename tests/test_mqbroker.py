# -*- coding: utf-8 -*-
# Copyright 2018-2019 GO FLEET SOLUTIONS S.R.L. See LICENSE for details.
"""
@package test_mqbroker

This package implements the mqbroker unit tests
"""

import sys
import unittest
from unittest.mock import patch

from mqbroker.main import parse_args

DEFAULT_ARGUMENTS = {'src_addrs': None,
                     'dst_addrs': None,
                     'topic_filter': None,
                     'verbose': False,
                     'show_version': False}



class TCMqbroker(unittest.TestCase):
    """ Implements unit tests for the mqbroker
    """
    def test_parse_args(self):
        """ Check the arguments parser
        """
        testargs = ["mqbroker"]
        with patch.object(sys, 'argv', testargs):
            arguments = parse_args()

            print(arguments)
            self.assertEqual(arguments, DEFAULT_ARGUMENTS)
