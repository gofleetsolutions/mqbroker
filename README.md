# mqbroker
Aggregate and dispatch ZeroMQ messages from various sockets.

MQBroker is a Python3 project using [ZeroMQ](http://zeromq.org/) to aggregate and dispatch messages from various ZeroMQ sockets. Typically used to subscribe from multiple IPC sockets and send data over a single TCP socket.

# Installation

Clone the source tree:
```sh
git clone https://gitlab.com/gofleetsolutions/mqbroker.git
```

Install mqbroker with:
```sh
python3 setup.py install
```

# Usage
From the command line, subscribe to two IPC sockets and publish data over TCP (port 55555):
```sh
mqbroker --src 'ipc:///tmp/source1' --src 'ipc:///tmp/source2' --dst 'tcp://*:55555*'
```

# Reports

* [Pylint report](https://gofleetsolutions.gitlab.io/mqbroker/quality-report/pylint-report.html)
* [Coverage report](https://gofleetsolutions.gitlab.io/mqbroker/coverage-report/index.html)
* [Unit tests report](https://gofleetsolutions.gitlab.io/mqbroker/test-report/test_report.html)
